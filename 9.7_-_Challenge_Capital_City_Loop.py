capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

import random

state_list = list(capitals_dict)

state_game = random.choice(state_list)
capital_game = capitals_dict[state_game].upper()
print(state_game, capital_game)

while True:
    user_capital = input(f"What is capital of {state_game}?: ").upper()
    if user_capital == capital_game:
        print("Bravo!")
        break
    elif user_capital == "EXIT":
        print("Game over")
        break 




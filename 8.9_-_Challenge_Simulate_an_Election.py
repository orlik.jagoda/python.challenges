import random

# wygrywa kandydat, który wygra w dwóch regionach
# losujemy, kto wygrywa w regionach: 1, 2, 3
# sprawdzamy kto wygrał w min. 2 regionach
# pętla ile razy wygrał kandydat 2 po 10_000 losowań wygrane/wybory * 100

candidates = ["candidate_A", "candidate_B"]
candidate_A_winning = 0
candidate_B_winning = 0
simulates_election = 10_000

def sprawdzenie(x):
    if x == ["candidate_A"]:
        print("candidate_A") 
    else:
        print("candidate_B")

for i in range(simulates_election):
    #losujemy kto wygrywa w regionie
    region_1 = random.choices(candidates, weights = [87, 13])
    region_2 = random.choices(candidates, weights = [65, 35])
    region_3 = random.choices(candidates, weights = [17, 83])
#    sprawdzenie(region_1) spr kodu na niższym range
#    sprawdzenie(region_2)
#    sprawdzenie(region_3)
    # jeżeli kandydat A wyra w 2 regionach dostaje 1 punkt
    if region_1 and region_2 == ["candidate_A"]:
        candidate_A_winning = candidate_A_winning + 1
    elif region_1 and region_3 == ["candidate_A"]:
        candidate_A_winning = candidate_A_winning + 1
    elif region_2 and region_3 == ["candidate_A"]:
        candidate_A_winning = candidate_A_winning + 1
    else:
        candidate_B_winning = candidate_B_winning + 1
#    print(candidate_A_winning) spr kodu na niższym range
#    print(candidate_B_winning)

wins_A_precentage = candidate_A_winning / simulates_election * 100
print(f"Candidate A wins {wins_A_precentage}% of the time out of \n\
      {simulates_election} simulations")

    


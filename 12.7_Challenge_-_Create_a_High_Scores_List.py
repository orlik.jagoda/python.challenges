from pathlib import Path
import csv

main_file_path = Path.home()/"practice_files" / "scores.csv"

score_list = []

with main_file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
        score_list.append(row)
        
high_score = {}

#print(score_list)
for item in score_list:
    name = item["name"]
    score = int(item["score"])
    if name not in high_score:
        high_score[name] = score
    else:
        if score > high_score[name]:
            high_score[name] = score

print(high_score)
for name in high_score:
    print(name, high_score[name])

new_file_path = Path.home()/"practice_files" / "high_scores.csv"
with new_file_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_score:
        row_dict = {"name":name, "high_score": high_score[name]}
        writer.writerow(row_dict)

        

#Temperature calculator

def conver_cel_to_far(x):
    """ function change the given value (x) in degrees \n
            Celsius to degrees Farenhait """
    far = float(x)* 9/5 + 32
    return far

def conver_far_to_cel(x):
    """ function change the given value (x) in degrees \n
            Farenhait to degrees Celsius """
    cel = (float(x)-32) * 5 / 9
    return cel    
      
user_temp_far = float(input("Enter a temperature in degrees F: "))
user_conver_to_cel = conver_far_to_cel(user_temp_far)
print(f"{user_temp_far} degrees F = {user_conver_to_cel:.2f} degrees C")

user_temp_cel = float(input("Enter a temperature in degrees C: "))
user_conver_to_far = conver_cel_to_far(user_temp_cel)
print(f"{user_temp_cel} degrees C = {user_conver_to_far:.2f} degrees F")

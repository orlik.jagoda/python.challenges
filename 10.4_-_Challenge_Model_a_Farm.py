
class Animal:
    def __init__(self, name, legs, color):
        self.name = name
        self.legs = legs
        self.color = color

    def __str__(self):
        return f"Hi my name is {self.name} and I walk on {self.legs} legs"

    def speak(self, sound):
        return f"{self.name} says {sound}"

    def type_of_food(self, food):
        return f"My favourite food is {food}"
    

class Pig(Animal):
    species = "livestoc"
    def speak(self, sound = "Oink Oink"):
        return super().speak(sound)

    def type_of_food(self, food = "cereals"):
        return super().type_of_food(food)

class Chicken(Animal):
    species = "poultry"
    def speak(self, sound = "Cluck Cluck"):
        return super().speak(sound)

    def type_of_food(self, food = "corn"):
        return super().type_of_food(food)
    
class Dog(Animal):
    species = "pets"
    def speak(self, sound = "Bark Bark"):
        return super().speak(sound)
    
    def type_of_food(self, food = "bones"):
        return super().type_of_food(food)


peppa= Pig("Peppa", 2, "pink")
normal_pig = Pig("Piglet", 4, "pink")

little_chicken = Chicken("Little Chicken", 2, "white")
standard_chicken = Chicken("Chicken", 2, "Orange")

scooby_Doo = Dog("Scooby Doo", 4, "Brown")
buddy = Dog("Buddy", 3, "Blac")

print(peppa)
print(peppa.species)
print(peppa.speak())
print(peppa.type_of_food())

print(normal_pig)
print(little_chicken.species)
print(standard_chicken.speak())

print(scooby_Doo.type_of_food())
print(buddy)

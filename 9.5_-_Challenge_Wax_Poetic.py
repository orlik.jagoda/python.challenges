nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]

verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]

adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]

prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]

adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]


import random

"""
def make_poem():
    noun1 = random.choice(nouns)
    noun2 = random.choice(nouns)
    noun3 = random.choice(nouns)
    verb1 = random.choice(verbs)
    verb2 = random.choice(verbs)
    verb3 = random.choice(verbs)
    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)
    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)
    adverb1 = random.choice(adverbs)
    if adj1[0] == ["a", "e", "o"]:
        a_an = "An"
    else:
        a_an = "A"
"""

def make_poem():
    noun = random.choices(nouns, k=3)
#    print(noun[0])
    verb = random.choices(verbs, k=3)
#    print(verb)
    adj = random.choices(adjectives,k=3)
#    print(adj)
    adj_a_an = adj[0]
#    print(adj_a_an)
    prep = random.choices(prepositions,k=2)
#    print(prep)
    adverb = random.choices(adverbs, k=1)
#    print(adverb)
    aeiou_list = ["a","e","i", "o", "u"]
    if adj_a_an[0] in aeiou_list:
        first = "An"
    else:
        first = "A"
 #   print(first)
    poem = f"""
    {first} {adj[0]} {noun[0]}

    {first} {adj[0]} {noun[0]} {verb[0]}{prep[0]} the {adj[1]} {noun[2]}
    {adverb[0]}, the {noun[0]} {verb[1]}
    the {noun[1]} {verb[2]} {prep[1]} a {adj[2]} {noun[2]}"""
    return poem

print(make_poem())


import random
repeat = 0
num_trail = 10_000

def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

for trial in range(num_trail):
    if coin_flip() == "heads":
        repeat = repeat + 1
        while coin_flip() == "heads":
            repeat = repeat + 1
        repeat = repeat + 1
#        print(f" heads first sum of reps: {repeat}")
    else: 
        repeat = repeat + 1
        while coin_flip() == "tails":
            repeat = repeat + 1
        repeat = repeat + 1
#       print(f" tails first sum of reps: {repeat}")
#    trial = trial + 1
#    print(trial)   

print(repeat / num_trail)

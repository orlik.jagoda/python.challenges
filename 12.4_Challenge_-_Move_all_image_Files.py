from pathlib import Path

main_dir = Path.home() /"dokuments"
main_dir.mkdir()

print(main_dir.exists())

image1 = main_dir / "image1.png"
image2 = main_dir / "image2.gif"
image3 = main_dir / "image3.png"
image4 = main_dir / "image4.png"

image_list = [image1, image2, image3, image4]

for i in image_list:
    i.touch()

for path in main_dir.iterdir():
    print(path)

images_dir = main_dir / "images"
images_dir.mkdir()
print(images_dir.exists())

for path in main_dir.glob("image?.*"):
    path.replace(images_dir / path.name)
